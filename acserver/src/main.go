package main

import (
	"flag"
	"fmt"
	"maginyecz/autocomplete/completer"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type termstruct struct {
	Term string `json:"term"`
}

func main() {
	similarity := *flag.Float64("similarity", 0.25, "Levenshtein tavolsag")
	port := *flag.String("port", ":8080", "Port")
	path := *flag.String("path", "/suggest", "Path")
	flag.Parse()

	ac := completer.NewCompleter(similarity)

	r := gin.Default()
	//r.Use(cors.Default())
	p := r.Group(path)

	p.GET("/status", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"hello": "world",
		})
	})

	p.DELETE("/", func(c *gin.Context) {
		var s termstruct
		c.BindJSON(&s)
		ac.Remove(s.Term)
		c.JSON(http.StatusOK, gin.H{
			"success": "deleted",
		})
	})

	p.POST("/", func(c *gin.Context) {
		var s termstruct
		c.BindJSON(&s)
		fmt.Println(s)
		//term := c.Query("term")
		fmt.Println(s.Term)
		if len(s.Term) < 3 {
			c.String(http.StatusNotAcceptable, "Term is too short")
			return
		}
		ac.Insert(s.Term)
		c.JSON(http.StatusOK, gin.H{
			"success": "added",
		})
	})

	p.GET("/", func(c *gin.Context) {
		term := strings.ToLower(c.Query("term"))
		ret := ac.Suggest(term)
		data := []string{}
		fmt.Println(ret)
		for _, v := range ret {
			data = append(data, v.Value)
		}
		c.JSON(http.StatusOK, data)
	})

	p.GET("/check", func(c *gin.Context) {
		d := ac.All()
		c.JSON(http.StatusOK, d)
	})

	p.GET("/export", func(c *gin.Context) {
		ac.Export()
		c.JSON(http.StatusOK, gin.H{
			"success": "exported",
		})
	})

	p.GET("/import", func(c *gin.Context) {
		ac.Import()
		c.JSON(http.StatusOK, gin.H{
			"success": "imported",
		})
	})

	r.Run(port)
}
