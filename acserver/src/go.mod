module maginyecz/autocomplete

go 1.13

require (
	github.com/alediaferia/prefixmap v1.0.1
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/mux v1.7.4 // indirect
	gopkg.in/alediaferia/stackgo.v1 v1.1.1 // indirect
)
