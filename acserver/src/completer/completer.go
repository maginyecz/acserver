package completer

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strings"
	"sync"

	"github.com/alediaferia/prefixmap"
)

type Completer struct {
	mux        sync.Mutex
	imap       map[string]uint64
	similarity float64
	datasource *prefixmap.PrefixMap
}

type Match struct {
	Value      string
	Similarity float64
}

func NewCompleter(similarity float64) *Completer {
	c := &Completer{
		similarity: similarity,
		datasource: prefixmap.New(),
		imap:       make(map[string]uint64),
	}
	c.Import()
	return c
}

func (c *Completer) Insert(s string) {
	_, ok := c.imap[s]
	if ok || len(s) < 3 {
		c.mux.Lock()
		c.imap[s]++
		c.mux.Unlock()
		return
	}
	c.imap[s] = 1
	parts := strings.Split(strings.ToLower(s), " ")
	for _, part := range parts {
		c.datasource.Insert(part, s)
	}
}

// removes the key from imap only,
// the suggester will ignore the change
// until the next import/export
func (c *Completer) Remove(s string) {
	if _, ok := c.imap[s]; !ok {
		return
	}
	c.mux.Lock()
	delete(c.imap, s)
	c.mux.Unlock()
}

func (c *Completer) Suggest(input string) []*Match {
	values := c.datasource.GetByPrefix(strings.ToLower(input))
	results := make([]*Match, 0, len(values))
	for _, v := range values {
		value := v.(string)
		s := ComputeSimilarity(len(value), len(input), LevenshteinDistance(value, input))
		if s >= c.similarity {
			m := &Match{value, s}
			results = append(results, m)
		}
	}
	return results
}

func (c *Completer) Import() {
	content, err := ioutil.ReadFile("./backup.json")
	if err != nil {
		fmt.Println(err)
		return
	}
	data := []string{}
	json.Unmarshal(content, &data)
	for _, v := range data {
		c.Insert(v)
	}
	fmt.Printf("Import ok, %d elem betoltve\n", len(data))
}

func (c *Completer) Export() error {
	f, err := os.Create("./backup.json")
	if err != nil {
		return err
	}
	defer f.Close()
	dataset := []string{}
	for k := range c.imap {
		dataset = append(dataset, k)
	}
	json.NewEncoder(f).Encode(dataset)
	return nil
}

func ComputeSimilarity(w1Len, w2Len, ld int) float64 {
	maxLen := math.Max(float64(w1Len), float64(w2Len))

	return 1.0 - float64(ld)/float64(maxLen)
}

func LevenshteinDistance(source, destination string) int {
	vec1 := make([]int, len(destination)+1)
	vec2 := make([]int, len(destination)+1)

	w1 := []rune(source)
	w2 := []rune(destination)

	// initializing vec1
	for i := 0; i < len(vec1); i++ {
		vec1[i] = i
	}

	// initializing the matrix
	for i := 0; i < len(w1); i++ {
		vec2[0] = i + 1

		for j := 0; j < len(w2); j++ {
			cost := 1
			if w1[i] == w2[j] {
				cost = 0
			}
			min := minimum(vec2[j]+1, vec1[j+1]+1, vec1[j]+cost)
			vec2[j+1] = min
		}

		for j := 0; j < len(vec1); j++ {
			vec1[j] = vec2[j]
		}
	}

	return vec2[len(w2)]
}

func minimum(value0 int, values ...int) int {
	min := value0
	for _, v := range values {
		if v < min {
			min = v
		}
	}
	return min
}

func (c *Completer) All() []string {
	d := []string{}
	for v, _ := range c.imap {
		d = append(d, v)
	}
	return d
}
