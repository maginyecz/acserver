package completer

import (
	"testing"
)

var c = NewCompleter(0.2)
var words = []interface{}{
	"bmw 1er",
	"bmw m2",
	"bmw 3er",
	"bmw 3 series",
	"bmw 330i",
	"bmw 320d",
	"bmw 320i",
	"bmw x4m",
	"bmw x5",
	"bmw x6",
	"lexus is200t",
	"lexus is300h",
	"lexus is350",
}

func TestSuggest(t *testing.T) {
	for _, v := range words {
		c.Insert(v.(string))
	}
	res := c.Suggest("lexu")
	if len(res) != 3 {
		t.Error("Rossz a suggest:", res)
	}
}
